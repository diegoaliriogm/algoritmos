
package MUNDO;

public class NodoBD{
    
    private NodoBD izq;
    private NodoBD der;
    private int info;
    //para poder pintarlo
    private int ID;

    public NodoBD getIzq() {
        return izq;
    }

    public void setIzq(NodoBD izq) {
        this.izq = izq;
    }

    public NodoBD getDer() {
        return der;
    }

    public void setDer(NodoBD der) {
        this.der = der;
    }

    public int getInfo() {
        return info;
    }

    public void setInfo(int info) {
        this.info = info;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    
    
}
