
package MUNDO;

public class ArbolBD{
    
    private NodoBD raiz;
    private int cardinalidad;
    
    //considerando que la logica del negocio se hiso el arbol nuevo para controlar la inserceciond de cada letra en el arbol
    
    public ArbolBD(){
        this.raiz = new NodoBD();
    }
    //metodo decorador del que va a insertar
    //establece los parametros iniciales
    //recibe una letra pero se hace string por el ch
    public boolean insert(String letra){
        //la omnncierto en array de 1 y 0 y mira si esta todos los signos del lenguaje
        int[]cad = this.encode(letra);
        //manda nulo si no esta definido 
        
        if(cad!=null){
            //inserta los valores del array en cada posicion del arbol
            return this.insert(this.raiz, cad, 0);
        }
        return false;
    }
    
    public String preOrden(){
        String izq = this.preOrden(this.raiz.getIzq());
        String der = this.preOrden(this.raiz.getDer());
        String cad = "X";
        
        if(!izq.isEmpty()&&!der.isEmpty()){
            return cad+" "+izq+" "+der;
        }
        if(!izq.isEmpty()){
            return cad+" "+izq;
        }
        return cad+" "+der;
        
    }
    
    private String preOrden(NodoBD n){
        if(n==null){
            return "";
        }
        String izq=this.preOrden(n.getIzq());
        String der=this.preOrden(n.getDer());
        String cad=n.getInfo()+":"+n.getID();
        
        if(!izq.isEmpty()&&!der.isEmpty()){
            return cad+" "+izq+" "+der;
        }
        if(!izq.isEmpty()){
            return cad+" "+izq;
        }
        return cad+" "+der;
    }
    
   //decorador izquierda, centro,derecha
    public String inOrden(){
        String izq = this.inOrden(this.raiz.getIzq());
        String der = this.inOrden(this.raiz.getDer());
        String cad = "X";
        if(!izq.isEmpty()&&!der.isEmpty()){
            return izq+" "+cad+" "+der;
        }
        if(!izq.isEmpty()){
            return izq+" "+cad;
        }
        return cad+" "+der;
    }
    
    //
    private String inOrden(NodoBD n){
        if(n==null){
            return "";
        }   
        String izq=this.inOrden(n.getIzq());
        String der=this.inOrden(n.getDer());
        String cad=n.getInfo()+":"+n.getID();
        
        if(!izq.isEmpty()&&!der.isEmpty()){
            return izq+" "+cad+" "+der;
        }
        if(!izq.isEmpty()){
            return izq+" "+cad;
        }
        return cad+" "+der;
    }
    
    //los hijos derechos son 1 y los izquierdos 0
    //cad es el arreglo de ceros y unos que esta llegando
    private boolean insert(NodoBD N, int[]cad, int x){
        //si x es igual al tamaño del arreglo la insertada fue exitosa y manda true
        if(x==cad.length){
            return true;
        }
        if(cad[x]==0){
            if(N.getIzq()==null){
                this.cardinalidad++;
                NodoBD temp = new NodoBD();
                temp.setInfo(cad[x]);
                temp.setID(this.cardinalidad);
                N.setIzq(temp);
            }
            return this.insert(N.getIzq(), cad, x+1);
        }
        if(N.getDer()==null){
            this.cardinalidad++;
            NodoBD temp = new NodoBD();
            temp.setInfo(cad[x]);
            temp.setID(this.cardinalidad);
            N.setDer(temp);
        }
        return this.insert(N.getDer(), cad, x+1);
    }
    
    private int[] encode(String letra){
        switch(letra){
            case "a":
                return new int[]{0,0,0,0,0,1};
            case "b":
                return new int[]{0,0,0,0,1,0};
            case "c":
                return new int[]{0,0,0,0,1,1};
            case "ch":
                return new int[]{0,0,0,1,0,0};
            case "d":
                return new int[]{0,0,0,1,0,1};
            case "e":
                return new int[]{0,0,0,1,1,0};
            case "f":
                return new int[]{0,0,0,1,1,1};
            case "g":
                return new int[]{0,0,1,0,0,0};
            case "h":
                return new int[]{0,0,1,0,0,1};
            case "i":
                return new int[]{0,0,1,0,1,0};
            case "j":
                return new int[]{0,0,1,0,1,1};
            case "k":
                return new int[]{0,0,1,1,0,0};
            case "l":
                return new int[]{0,0,1,1,0,1};
            case "m":
                return new int[]{0,0,1,1,1,0};
            case "n":
                return new int[]{0,0,1,1,1,1};
            case "ñ":
                return new int[]{1,1,0,0,0,0};
            case "o":
                return new int[]{1,1,0,0,0,1};
            case "p":
                return new int[]{1,1,0,0,1,0};
            case "q":
                return new int[]{1,1,0,0,1,1};
            case "r":
                return new int[]{1,1,0,1,0,0};
            case "s":
                return new int[]{1,1,0,1,0,1};
            case "t":
                return new int[]{1,1,0,1,1,0};
            case "u":
                return new int[]{1,1,0,1,1,1};
            case "v":
                return new int[]{1,1,1,0,0,0};
            case "w":
                return new int[]{1,1,1,0,0,1};
            case "x":
                return new int[]{1,1,1,0,1,0};
            case "y":
                return new int[]{1,1,1,0,1,1};
            case "z":
                return new int[]{1,1,1,1,0,0};
            case "\n":
                return new int[]{1,1,1,1,0,1};
            case " ":
                return new int[]{1,1,1,1,1,0};
        }
        return null;
    }
    
    
    
}
